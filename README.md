# Participants
- Mário
- Filipe

# Technical specs

| Name                 | Value                                                                                   |
| -------------------- | --------------------------------------------------------------------------------------- |
| VMWare version       | [16.X](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html) |
| Linux distribution   | [Ubuntu 20.04](https://ubuntu.com/download/server)                                      |
| Language             | EN-US                                                                                   |
| Keyboard layout      | fr-CH                                                                                   |
| Hostname             | ubuntu-cld1                                                                             |
| Open ports           | 22 (ssh), 80 (nginx)                                                                    |
| PHP version          | 7.4                                                                                     |
| MariaDB version      | 10.3.31                                                                                 |
| Processors           | 4                                                                                       |
| Cores per processors | 2                                                                                       |
| RAM                  | 4096 MB                                                                                 |
| SCSI Controller      | LSI Logic                                                                               |
| Virtual disk type    | SCSI                                                                                    |
| Disk size            | 30GB                                                                                    |

## Network
| Name          | Value                                                       |
| ------------- | ----------------------------------------------------------- |
| Connection    | NAT                                                         |
| Interface     | ens33                                                       |
| Configuration | dhcp (IPv4)                                                 |
| Mirror        | [Switzerland mirror](https://ch.archive.ubuntu.com/ubuntu/) |

## Profile setup
| Name              | Value        |
| ------------------|--------------|
| User's name       | Mario        |
| Server's hostname | ubuntu-cld1  |
| Username          | mario        |
| Password          | \<PASSWORD\> |


# Users isolation
## SSH connection
In order to easily grant or revoke ssh access, we have created a group called 'ssh_users'. Only users that belong to this group will be able to establish an SSH connection.

This is done by adding **AllowedGroups ssh_users** to **/etc/ssh/sshd_config**.

I.e: if you want to allow the user called 'client1' to connect to the ubuntu server, type:
```sh
# usermod -aG ssh_users client1
```

Or, if you want to revoke client1's ssh access, type:
```sh
# gpasswd -d client ssh_users
```

## PHP-FPM

Each new user has a dedicated virtual host. In order to properly isolate each user, we have to make sure that the php process that handles http requests for the user's virtual host runs as a **normal user (neither root nor www-data)**.

This can be done by creating a php-fpm pool for the new user.

By setting the **user and group**, the php-fpm process will be run as a normal user. But, if we realy want to isolate every user, we have to tell php-fpm that every pool will have its own socket file (so that other php-fpm process cannot know what type of commands/requests a process is handling).

Finally, set some environment variables such as **PATH** and **HOSTNAME** so that program's full path does not have to be specified.

Bellow, there is an example of what needs to be modified from the default pool (**/etc/php/7.4/fpm/pool.d/www\.conf**) in order to set the correct permissions for php-fpm processes.

#### **`/etc/php/7.4/fpm/pool.d/<USERNAME>.conf`**
```conf
; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
user = <USERNAME>
group = <USERNAME>

; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
listen = /run/php/php-fpm_<USERNAME>.sock

; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server. Many
; BSD-derived systems allow connections regardless of permissions.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = www-data
listen.group = www-data

; Pass environment variables like LD_LIBRARY_PATH. All \$VARIABLEs are taken from
; the current environment.
; Default Value: clean env
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
```

Where **\<USERNAME\>** is the user's nickname in lowercase (ie. mario => \<USERNAME\> means **mario**) and **www-data** is the nginx's user.

Do not forget to tell nginx that all incoming requests (for the user's virtual host) must be handled by the user's php-fpm pool by setting **fastcgi_pass unix:/run/php/php-fpm_\<USERNAME\>.sock;**

#### **`/etc/nginx/sites-available/<USERNAME>.conf`**
```conf
location ~ \.php$ {
    try_files \$uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/run/php/php-fpm_<USERNAME>.sock;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    include fastcgi_params;
}
```

## User's home
When hosting a website, **/var/www** or **/srv/http** (which is the recommended way as per the [FHS](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s17.html)) ared used as the document root of a virtual host.

But, since we are hosting multiple websites for multiple users, we set the DOCUMENT_ROOT as **$HOME/www**. Also, we had to **set user's home directories rights to 750** (the default permissions are 755).

By removing any access for all the users that do not belong to the client's group, we make sure that all users are isolated and that none can see other user's personnal files.

But, before that, we need to setup **/home** permissions. We wanted the nginx's user to  only enter the user's home directory. This is easily done by setting **/home/\<USERNAME\>** permissions to **710**. Thus, the nginx's user cannot list **/home/\<USERNAME\>** content.

So, for each new user, a group called 'www-data_\<USERNAME\>' will be created. In this group, only the
newly created user and nginx's user belong to. This allows us to set grained permissions without having
to create 'complex' [acls](https://www.redhat.com/sysadmin/linux-access-control-lists).

In order to ensure that new user's home directories will have the same permissions, we created an acl.
```sh
setfacl -d -m u::rwx -m g::--x -m o::--- "/home"
```

Also, we set a sticky bit for the groups' permission to that all files that will be created under **/home/\<USERNAME\>/www** will belong to **www-data_\<USERNAME\>**.

The acl we set revokes any rights for users that do not belong to **www-data_\<USERNAME\>** group.

```sh
chmod g+s "/home/<USERNAME>/www"
setfacl -d -m u::rwx -m g::r-x -m o::--- "/home/<USERNAME>/www"
```

**Side note: We could use chroots by modifying ssh's configuration file, but this was outside of this project's scope.**

# Database
Every time a new user is created, a new database account is created as well as an empty database. The new user can do everything with the database we created for him. That's it. He only has access to the database created for him and so, he cannot see other user's data.

This is done with the following **SQL** queries:
```sql
CREATE DATABASE <USERNAME>;
CREATE USER <USERNAME>@localhost IDENTIFIED BY '<PASSWORD>';
GRANT ALL PRIVILEGES ON <USERNAME>.* TO <USERNAME>@localhost;
FLUSH PRIVILEGES;
```

# Step by step procedure
## Installation
First, grab the latest lts's iso on Ubuntu's [website](https://ubuntu.com/download/server).

### Partition scheme
![Partition scheme - part 1](img/partition_scheme_1.png)
![Partition scheme - part 2](img/partition_scheme_2.png)

**Do not install any packages from the ubuntu's installation wizard**
<div style="page-break-after: always;"></div>

**IMPORTANT - Ubuntu's installation wizard adds the user it creates to the sudoers group. It also installs sudo by default.**

## Post-installation
**IMPORTANT - The following commands should be run as 'mario' (which is an admin user that belongs to the sudoers group)**.

Before doing anything, make sure your system is up-to-date.
```sh
$ sudo apt update && sudo apt upgrade
```

### ssh
```sh
$ sudo apt install --no-install-recommends ssh
```

Create **ssh_users** group. Only users that belong to this group will be able to connect to this machine using ssh.
```sh
$ sudo addgroup ssh_users
```

Do not forget to add your current user to the **ssh_users** group, otherwise you won't be able to connect
through ssh:

```sh
$ sudo usermod -aG ssh_users <USERNAME>
```

**If you created an user following [technical-specs](#technical-specs), \<USERNAME\> is mario**.

Modify **sshd's** config file as root and add the following after **Include /etc/ssh/sshd_config.d/*.conf**:
#### **`/etc/ssh/sshd_config`**
```conf
AllowGroups ssh_users
```

Finally, restart ssh:

```sh
$ sudo systemctl restart sshd
```

### Nginx
Install nginx and enable its service:

```sh
$ sudo apt install --no-install-recommends nginx
$ sudo systemctl enable --now nginx
```

### PHP support
Install PHP and PHP FPM 

```sh
$ sudo apt install --no-install-recommends php-fpm php-common php-cli
$ sudo systemctl enable --now php7.4-fpm
```

<div style="page-break-after: always;"></div>

### Security
It is important that we prevent Nginx from passing requests to the PHP-FPM backend if the file does not exists, allowing us to prevent arbitrarily script injection.

We can fix this by setting the cgi.fix_pathinfo directive to 0 within our php.ini file.

As root (either using sudo or su), set fix_path to 0:

#### **`/etc/php/7.4/fpm/php.ini`**
```conf
cgi.fix_pathinfo=0
```

Finally, restart php-fpm in order to apply modifications:

```sh
$ sudo systemctl restart php7.4-fpm
```

### MariaDB
```sh
$ sudo apt install --no-install-recommends mariadb-server
$ sudo systemctl enable --now mariadb
```

<div style="page-break-after: always;"></div>

### Create a new user
Since we often create new users, it is better to automate it using
a shell script.
The script we wrote will create a new user, generate an nginx config file (virtual host), create a database for him and isolate him (make sure he cannot check other's users files).

As the server admin (user => mario if you followed the #technical-specs), clone the git repository bellow and execute the shell script as root:

Before creating launching the script, install **acl** package which allows to manage acls:
```sh
$ sudo apt install --no-install-recommends acl
```

```sh
$ cd ~ && mkdir git && cd git
$ git clone https://github.com/Thynkon/cld1.git
$ cd cld1
$ sudo ./cld1 -u <USERNAME>
```

**Side note**: By default, git is installed. This might indicate that Ubuntu install a lot of **bloatware**. It would be interesting if we could list a list of packages that are already installed but shouldn't be.

**IMPORTANT - We pass the users's password when creating a new database user for simplicity. We are aware that in a production environment, we should create a file containing the passwords (which is only readable by certain users) and tell both useradd and mysql to read the password from that file. Also, we read from the STDIN the password for the new user. As for the database user, we should create a file containing the password.**

# How to deploy a web site
As mentioned in the [users isolation section](#users-home), nginx will look for files under **~/www/**. So, all the user has to to in order to setup a webapp is to copy his project under that folder.

There are two ways. He can either copy files from his machine to the web server using **scp** or connect to the web server using an ssh connection and clone a git repository on the nginx document root.

Also, if the client wants to connect to his database account, here are the credentials he has to use:


| Database      | Username      | Password              |
|---------------|---------------|-----------------------|
| \<USERNAME\>  | \<USERNAME\>  | \<USERNAME\>_password |

Where **\<USERNAME\>** is the user's nickname in lowercase (ie. mario => \<USERNAME\> means **mario**).

## Scp method
From the client's machine type:

```sh
$ scp -pr ./<WEB_SITE_FOLDER>/* <USERNAME>@<HOSTNAME>:~/www
```

<div style="page-break-after: always;"></div>

## Git clone method
From the client's machine type:

```sh
$ ssh <USERNAME>@<HOSTNAME>
```

Once you are connected to the ssh server, clone the git repository under **~/www**.

```sh
$ git clone <GIT_REPO_URL> ~/www
```

Now that you have copied your project to the web server, add the following to the hosts file so you can connect to your new website from your local network (in a production server, we wouldn't have to modify the hosts file because we would use a domain name).

#### **`/etc/hosts`**
```config
<WEB_SERVER_IP>     <USERNAME>.local
```

Where **\<USERNAME\>** is the user's nickname in lowercase (ie. mario => \<USERNAME\> means **mario**) and **\<WEB_SERVER_IP\>** is the ip address of the virtual machine you have just setup.

And that's it. You should have now a working website that has a database access.
